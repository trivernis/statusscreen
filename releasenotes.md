<<<<<<< HEAD
# **Changes in version 1.1.0**
## New Classes
* ***Image*** class
  * An image from a file that can optinally refresh every x seconds
* ***Webcam*** class
  * A simple webcam screen output
  * The webcam can be selected with `number="int"`
## Modified Classes
* ***Console*** class
  * Added `--` and `##` to the syntax highlighting
## Other changes
* Added ***Image*** pattern description
* Added the ***Webcam*** pattern description
* removed `feedparse` from `graphiclib.py`
* Added an interval to the webcam
  * The interval is set with `interval="float"`
* Modified the `update.sh` script
  * The current user gains read and write access when executing the update script
  * The name of the Repository is now stored in a variable
* Modified the `install.sh` script
  * The install-script is shorter
  * Included `psycopg2` in the install script
  * Included `optparse` in the install script
* Added a rss-feedreader program `prog/feedreader.py`
* Added special programs for the raspberry pi in `prog/pi-programs` *(working on more)*
* Added the Consolas font at `lib/fonts/Consolas.ttf`
=======
# **Changes in version 1.2.0**
## New Classes
* ***Center/NotificationCenter*** class
   * Just like the console class but with Notification-Items instead of lines.
## Modified Classes
* ***SQLGraph*** class
   * uses Titlebar class instead of self generated titlebar
   * can now be maximized
* ***Console*** class
   * uses Titlebar class instead of self generated titlebar
   * can be cleared with the cross button
* ***Clock*** class
   * refreshes every second instead of every frame
* ***Timer*** class
   * refreshes every second instead of every frame
## Other changes
* Added graphic_additions
   * Added Button class
   * Added TextButton class
   * Added ButtonGenerator
   * Added Buttons Enum
   * Added Titlebar class
   * Added NotificationCard class
* Added flipping to newer classes
* Working with hasattr instead of trying and catching
* Removed moving of sprites
* Modified parser
   * Reads sprite types from json file to shorten the parsing
* Some performance fixing
   * generators instead of lists
* Reformatted Code
>>>>>>> 1.2.0
