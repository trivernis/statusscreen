import feedparser, optparse, bs4, time, re

def main(options):
    running = True
    printed = []
    while running:
        try:
            feed = feedparser.parse(options.url)
            title = feed['channel']['title']
            items = reversed(feed['items'])
            for item in items:
                if item not in printed:
                    printed.append(item)
                    i_title = item['title']
                    try:
                        i_summary = item['summary']
                    except KeyError:
                        i_summary = None
                    try:
                        i_date = item['date_parsed']
                    except KeyError:
                        i_date = None
                    if options.date and i_date:
                        print('#{}.{}.{} {}:{}:{}#'.format(i_date[2], i_date[1],i_date[0],i_date[3],i_date[4],i_date[5]))
                    if options.title:
                        print("--{}--".format(title))
                    print("-{}-".format(i_title))
                    if options.content:
                        soup = bs4.BeautifulSoup(i_summary,"html.parser")
                        soup.prettify()
                        text = soup.get_text()
                        rx = re.compile('\W+')
                        text = rx.sub(' ', text).strip()
                        text = text.replace('Continue', '')
                        text = text.replace('reading', '')
                        if len(text)>0:
                            text += '.'
                            print(text)
                    print()
        except KeyboardInterrupt:
            pass
        except KeyError:
            print('[CRITICAL] Key Error. Is this a rss-feed?')
        except Exception as e:
            print('[ERROR] {}'.format(e))
        if running:
            time.sleep(60)

if __name__=='__main__':
    parser = optparse.OptionParser()
    parser.add_option('-u', '--url', dest='url', help='rss-url', type='string')
    parser.add_option('-c', '--content', dest='content', action="store_true", default=False, help='rss-url')
    parser.add_option('-t', '--title', dest='title', action="store_true", default=False, help='rss-url')
    parser.add_option('-d', '--date', dest='date', action="store_true", default=False, help='rss-url')
    options, args = parser.parse_args()
    main(options)