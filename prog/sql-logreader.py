import psycopg2, optparse, time

def login(host, port, user, password, database):
    conString = "dbname={} host={} port={} user={} password={} application_name={}".format(database, host, port, user,                                                                                     password, 'sql-logreader')
    conn = psycopg2.connect(conString)
    cursor = conn.cursor()
    return (conn, cursor)

def main(options):
    conn, cursor = login(options.host, options.port, options.user, options.password, options.database)
    lastid = 0
    try:
        running = True
        cursor.execute('SELECT COUNT(*) FROM {}'.format(options.table))
        count = cursor.fetchone()[0]
        cursor.execute("SELECT id, level, message FROM {} OFFSET {}".format(options.table, count-50))
        while running:
            rows = cursor.fetchall()
            if len(rows)>0:
                lastid = rows[-1][0]
                for row in rows:
                    print('[{}] {}'.format(row[1], row[2]))
            cursor.execute("SELECT id, level, message FROM {} WHERE id={}".format(options.table, lastid+1))
            time.sleep(10)

    except Exception as e:
        print('[CRITICAL]-[PROGRAM] {}'.format(e))

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('-H', '--host', dest = 'host', help = 'host of the postgresql-server', type='string')
    parser.add_option('-P', '--port', dest='port', help='port of the postgresql-server', type='int')
    parser.add_option('-u', '--user', dest='user', help='the username to login', type='string')
    parser.add_option('-p', '--password', dest='password', help='the password to login', type='string')
    parser.add_option('-d', '--database', dest='database', help='the used database on the server', type='string')
    parser.add_option('-t', '--table', dest='table', help='the used database on the server', type='string')
    options, args = parser.parse_args()
    main(options)