#!/usr/bin/env bash
name=statusscreen
cd ..
if [ -d "$name/" ];then
    sudo git clone https://bitbucket.org/trivernis/$name.git $name-update
    sudo rsync -a --exclude 'lib/configuration/sql.ini' --exclude './interfaces/interface.xml' --exclude './config/Settings.ini' ./$name-update/ ./$name/
    sudo rm -r ./$name-update/
    sudo chmod -R u+rw ./$name/
    exit 0
else
    sudo git clone https://bitbucket.org/trivernis/$name.git
    sudo chmod -R u+rw ./$name/
    exit 0
fi
