import psycopg2
import configparser


def get_config(group, keys):
    dict = {}
    for key in keys:
        dict[key] = None
    configfile = configparser.ConfigParser()
    configfile.read('./lib/configuration/sql.ini')
    if group in configfile.keys():
        gconfig = configfile[group]
        for key in keys:
            if key in gconfig.keys():
                dict[key] = gconfig[key]
    return dict


def login(database='default', name='python', config='POSTGRESQL'):
    dict = get_config(config, ['adress', 'port', 'user', 'password'])

    adress = dict['adress']
    port = int(dict['port'])

    user = dict['user']
    password = dict['password']

    conS = "dbname={} host={} port={} user={} password={} application_name={}"
    conS = conS.format(database, adress, port, user, password, name)
    conn = psycopg2.connect(conS)
    cursor = conn.cursor()
    return (conn, cursor)