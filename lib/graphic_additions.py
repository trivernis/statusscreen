import pygame
import math
from enum import Enum
from pygame import transform as ptr


def render_text_line(image, color, font, text, pos=(0, 0)):
    render_text = font.render(text, 1, color)
    image.blit(render_text, pos)


def test_sys_font(fontname):
    try:
        pygame.font.Font(fontname, 25)
        return False
    except OSError:
        pygame.font.SysFont(fontname, 25)
        return True


# Buttons

class Button(pygame.sprite.Sprite):
    """A standard button"""

    def __init__(self, position, default_img, screen_pos=None, pressed_img=None, size=None, function=None):
        pygame.sprite.Sprite.__init__(self)
        self.default_img = default_img
        if pressed_img:
            self.pressed_img = pressed_img
        else:
            self.pressed_img = default_img
        self.image = default_img
        if size:
            self.resize(size)
        self.rect = self.image.get_rect()
        if screen_pos:
            self.rect.topleft = screen_pos
        else:
            self.rect.topleft = position
        self.pressed = False
        self.shown = True
        self._mousepressed = False
        self.position = position
        self.function = function

    def resize(self, size):
        self.default_img = ptr.scale(self.default_img, size)
        self.pressed_img = ptr.scale(self.pressed_img, size)
        self.image = ptr.scale(self.image, size)

    def clicked(self):
        if self.shown:
            mousepos = pygame.mouse.get_pos()
            if bool(pygame.mouse.get_pressed()[0]):
                if self.rect.collidepoint(mousepos) and not self._mousepressed:
                    if not self.pressed:
                        self.image = self.pressed_img
                        self.pressed = True
                        if self.function:
                            self.function()
                    return True
                else:
                    self._mousepressed = True
            else:
                self._mousepressed = False
            if self.pressed:
                self.image = self.default_img
                self.pressed = False
        return False

    def draw(self, surface):
        if self.shown:
            surface.blit(self.image, self.position)

    def hide(self):
        self.shown = False

    def show(self):
        self.shown = True


class TextButton():
    """A Button with text rendered on it"""

    def __init__(self, position, size, string, fontsize=25, fontname='Arial', style=None):
        if test_sys_font(fontname):
            font = pygame.font.SysFont(fontname, fontsize)
        else:
            font = pygame.font.Font(fontname, fontsize)
        if style:
            img = self.render_img(string, size, font, color=style['color'], bcolor=style['back-color'])
            img_clicked = self.render_img(string, size, font, color=style['color:active'],
                                          bcolor=style['back-color:active'])
        else:
            img = self.render_img(string, size, font, bcolor=(0, 0, 0))
            img_clicked = self.render_img(string, size, font, bcolor=(100, 100, 100))
        self.button = Button(position, img, pressed_img=img_clicked)

    def render_img(self, string, size, font, bcolor=(0, 0, 0), color=(255, 255, 255)):
        image = pygame.Surface(size)
        image.fill(bcolor)
        render_text_line(image, color, font, string)
        return image

    def clicked(self):
        return self.button.clicked()


class Buttons(Enum):
    close = 0
    max = 1
    min = 2
    refresh = 3


class ButtonGenerator():
    """Generate some standard buttons with custom colors"""

    def __init__(self, style=None, *args, **kwargs):
        default = {
            'color': (255, 255, 255),
            'color:active': (0, 255, 255),
            'back-color': (0, 0, 0),
            'back-color:active': (50, 50, 50)
        }
        if style:
            self.style = style
        elif kwargs:
            self.style = kwargs
        else:
            self.style = default

    def generate_button(self, position, size, type, screenpos=None, function=None):
        if type == Buttons.close:
            return self._gen_close_button(position, size, screenpos, function)
        elif type == Buttons.max:
            return self._gen_max_button(position, size, screenpos, function)
        elif type == Buttons.min:
            return self._gen_min_button(position, size, screenpos, function)
        elif type == Buttons.refresh:
            return self._gen_refr_button(position, size, screenpos, function)

    def _gen_close_button(self, pos, size, screenpos, function):
        if not screenpos:
            screenpos = pos
        img = pygame.Surface(size)
        a_img = pygame.Surface(size)
        img.fill(self.style['back-color'])
        a_img.fill(self.style['back-color:active'])
        sub_x = size[0] / 4
        sub_y = size[1] / 4
        sp1 = (sub_x, sub_y)
        sp2 = (size[0] - sub_x, sub_y)
        ep1 = (size[0] - sub_x, size[1] - sub_y)
        ep2 = (sub_x, size[1] - sub_y)
        pygame.draw.line(img, self.style['color'], sp1, ep1, 3)
        pygame.draw.line(img, self.style['color'], sp2, ep2, 3)
        pygame.draw.line(a_img, self.style['color:active'], sp1, ep1, 3)
        pygame.draw.line(a_img, self.style['color:active'], sp2, ep2, 3)
        return Button(pos, img, pressed_img=a_img, screen_pos=screenpos, function=function)

    def _gen_max_button(self, pos, size, screenpos, function):
        if not screenpos:
            screenpos = pos
        img = pygame.Surface(size)
        a_img = pygame.Surface(size)
        img.fill(self.style['back-color'])
        a_img.fill(self.style['back-color:active'])
        sub_x = size[0] / 8
        sub_y = size[1] / 8
        ssub_x = size[0] / 4
        ssub_y = size[1] / 4
        lines = []
        tl = (sub_x, sub_y)
        br = (size[0] - sub_x, size[1] - sub_y)
        tr = (size[0] - sub_x, sub_y)
        bl = (sub_x, size[1] - sub_y)
        # top
        line = tl, (tl[0], tl[1] + ssub_y)
        lines.append(line)
        line = tl, (tl[0] + ssub_x, tl[1])
        lines.append(line)
        line = tr, (tr[0] - ssub_x, tr[1])
        lines.append(line)
        line = tr, (tr[0], tr[1] + ssub_y)
        lines.append(line)
        # bottom
        line = bl, (bl[0], bl[1] - ssub_y)
        lines.append(line)
        line = bl, (bl[0] + ssub_x, bl[1])
        lines.append(line)
        line = br, (br[0] - ssub_x, br[1])
        lines.append(line)
        line = br, (br[0], br[1] - ssub_y)
        lines.append(line)
        for line in lines:
            pygame.draw.line(img, self.style['color'], line[0], line[1], 3)
            pygame.draw.line(a_img, self.style['color:active'], line[0], line[1], 3)
        return Button(pos, img, pressed_img=a_img, screen_pos=screenpos, function=function)

    def _gen_min_button(self, pos, size, screenpos, function):
        if not screenpos:
            screenpos = pos
        img = pygame.Surface(size)
        a_img = pygame.Surface(size)
        img.fill(self.style['back-color'])
        a_img.fill(self.style['back-color:active'])
        sub_x = size[0] / 4
        sub_y = size[1] / 4
        ssub_x = size[0] / 4
        ssub_y = size[1] / 4
        lines = []
        tl = (sub_x, sub_y)
        br = (size[0] - sub_x, size[1] - sub_y)
        tr = (size[0] - sub_x, sub_y)
        bl = (sub_x, size[1] - sub_y)
        # top
        line = (0, ssub_y), tl
        lines.append(line)
        line = (ssub_x, 0), tl
        lines.append(line)
        line = (size[0] - ssub_x, 0), tr
        lines.append(line)
        line = (size[0], ssub_y), tr
        lines.append(line)
        # bottom
        lines.append(line)
        line = (0, size[1] - ssub_y), bl
        lines.append(line)
        line = (ssub_x, size[1]), bl
        lines.append(line)
        line = (size[0] - ssub_x, size[1]), br
        lines.append(line)
        line = (size[0], size[1] - ssub_y), br
        lines.append(line)
        for line in lines:
            pygame.draw.line(img, self.style['color'], line[0], line[1], 3)
            pygame.draw.line(a_img, self.style['color:active'], line[0], line[1], 3)
        return Button(pos, img, pressed_img=a_img, screen_pos=screenpos, function=function)

    def _gen_refr_button(self, pos, size, screenpos, function):
        if not screenpos:
            screenpos = pos
        img = pygame.Surface(size)
        a_img = pygame.Surface(size)
        img.fill(self.style['back-color'])
        a_img.fill(self.style['back-color:active'])
        sub_x = size[0] / 8
        sub_y = size[1] / 8
        ssub_x = size[0] / 8
        ssub_y = size[1] / 8
        p = size[0] / 2, ssub_y
        lines = []
        line = ((size[0] / 2) - ssub_x, 0), p
        lines.append(line)
        line = ((size[0] / 2) - ssub_x, size[1] / 3), p
        lines.append(line)
        for line in lines:
            pygame.draw.line(img, self.style['color'], line[0], line[1], 3)
            pygame.draw.line(a_img, self.style['color:active'], line[0], line[1], 3)
        arr_rect = pygame.Rect(sub_x, sub_y, size[0] * 0.75, size[1] * 0.75)
        pygame.draw.arc(img, self.style['color'], arr_rect, math.pi * 0.5, math.pi * 2, 3)
        pygame.draw.arc(a_img, self.style['color:active'], arr_rect, math.pi * 0.5, math.pi * 2, 3)
        return Button(pos, img, pressed_img=a_img, screen_pos=screenpos, function=function)


# Other

class Titlebar:

    def __init__(self, position, size, buttons, screenpos=None, style=None, title=None, maxsize=None, **kwargs):
        if style:
            self.style = style
        elif kwargs:
            self.style = kwargs
        else:
            self.style = None

        self.size = size
        if maxsize:
            self.maxsize = maxsize
        else:
            self.maxsize = size
        if screenpos:
            self.scrpos = screenpos
        else:
            self.scrpos = position
        self.image = pygame.Surface(size)
        self.image.fill(self.style['back-color'])
        self.rect = self.image.get_rect()
        self.rect.topleft = position
        self._get_buttons(buttons)
        self._set_font()
        self.clicked = []
        self.set_title(title)

    def _get_buttons(self, buttons):
        self.buttons = []
        size = self.rect.size
        g_style = {
            'color': self.style['color'],
            'color:active': self.style['back-color'],
            'back-color': self.style['back-color'],
            'back-color:active': self.style['color']
        }
        generator = ButtonGenerator(style=g_style)
        b_size = size[1], size[1]
        position = size[0] - b_size[0], 0
        tl = self.scrpos[0]
        for button in buttons:
            scrpos = tl + position[0], self.scrpos[1]
            b = generator.generate_button(position, b_size, button[0], screenpos=scrpos, function=button[1])
            if b:
                self.buttons.append(b)
                b.draw(self.image)
                position = position[0] - b_size[0], position[1]
            if position[0] < 0 and True:
                break

    def _set_font(self):
        fontname = self.style['fontname']
        fontsize = self.style['fontsize']

        if test_sys_font(fontname):
            self.font = pygame.font.SysFont(fontname, fontsize)
        else:
            self.font = pygame.font.Font(fontname, fontsize)

    def set_title(self, title):
        font = self.font
        color = self.style['color']
        self.image.fill(self.style['back-color'])
        render_text_line(self.image, color, font, title, pos=(2, 2))
        for button in self.buttons:
            button.draw(self.image)

    def set_max(self, max):
        if max:
            self.rect.size = self.maxsize
            img = self.image.copy()
            self.image = pygame.Surface(self.maxsize)
            self.image.fill(self.style['back-color'])
            self.image.blit(img, (0, 0))
            for button in self.buttons:
                tl = button.rect.topleft
                button.rect.topleft = tl[0], 0
        else:
            self.rect.size = self.size
            img = self.image.copy()
            self.image = pygame.Surface(self.size)
            self.image.blit(img, (0, 0))
            for button in self.buttons:
                tl = button.rect.topleft
                button.rect.topleft = tl[0], self.scrpos[1]
        self.update()

    def set_screen_pos(self, position):
        before = self.scrpos
        change = position[0] - before[0], position[1] - before[1]
        self.scrpos = position
        for button in self.buttons:
            tl = button.rect.topleft
            button.rect.topleft = tl[0] + change[0], tl[1] + change[1]

    def update(self):
        updated = False
        for button in self.buttons:
            if not button.shown:
                sf = pygame.Surface(button.rect.size)
                sf.fill(self.style['back-color'])
                self.image.blit(sf, button.rect)
            if button.clicked():
                if button not in self.clicked:
                    self.clicked.append(button)
                    button.draw(self.image)
                    updated = True
            elif button in self.clicked:
                self.clicked.remove(button)
                button.draw(self.image)
                updated = True
        return updated

    def draw(self, surface):
        surface.blit(self.image, self.rect)


class NotificationCard:
    """This card is used in the notification center"""

    def __init__(self, position, size, message, title=None, screenpos=None, style=None):
        default_style = {
            'color': (255, 255, 255),
            'back-color': (0, 0, 0),
            'fontname': 'Calibri',
            'fontsize': 25
        }
        if style:
            self.style = style
        else:
            self.style = default_style
        self.__font_init()
        self.image = pygame.Surface(size)
        self.rect = self.image.get_rect()
        self.rect.topleft = position
        self.scrpos = screenpos
        if not self.scrpos:
            self.scrpos = position
        self.closed = False
        self.exp_clicked = False
        self.__title_bar_init(title)
        self._draw_message(message)

    def __font_init(self):
        fontname = self.style['fontname']
        fontsize = self.style['fontsize']
        if test_sys_font(fontname):
            self.font = pygame.font.SysFont(fontname, fontsize)
        else:
            self.font = pygame.font.Font(fontname, fontsize)

    def __title_bar_init(self, title):
        size = self.rect.size
        title_style = {
            'color': self.style['back-color'],
            'back-color': self.style['color']
        }
        title_style['fontname'] = self.style['fontname']
        title_style['fontsize'] = int(self.style['fontsize'] * 1.2)
        buttons = []
        buttons.append((Buttons.close, self.close))
        buttons.append((Buttons.max, self.expand))
        if test_sys_font(title_style['fontname']):
            tfont = pygame.font.SysFont(title_style['fontname'], title_style['fontsize'])
        else:
            tfont = pygame.font.Font(title_style['fontname'], title_style['fontsize'])
        self.theight = tfont.size('|')[1]
        if title:
            self.titlebar = Titlebar((0, 0), (size[0], self.theight), buttons, style=title_style, title=title,
                                     screenpos=self.scrpos)
        else:
            self.titlebar = Titlebar((0, 0), (size[0], self.theight), buttons, style=title_style, screenpos=self.scrpos)
        self.titlebar.draw(self.image)

    def _draw_message(self, message: str):
        re_size = self.rect.size
        size = re_size
        font = self.font
        color = self.style['color']
        if self.titlebar:
            size = re_size[0], re_size[1] - self.theight
        message_img = pygame.Surface(size)
        message_img.fill(self.style['back-color'])
        inner_size = int(size[0] - 6), int(size[1] - 6)
        pos = int(3), int(3)
        # fit words into line
        if font.size(message)[0] > inner_size[0]:
            words = message.split()
            count_list = []
            word_height = font.size('|')[1]
            linecount = math.floor(inner_size[1] / word_height)
            word_sizes = (font.size(word + " ")[0] for word in words)
            sum_size = 0
            count = 0
            # go sum of sizes must fit to line
            for wsize in word_sizes:
                if wsize + sum_size > inner_size[0]:
                    if len(count_list) + 1 > linecount:
                        break
                    count_list.append(count)
                    sum_size = 0
                    count = 0
                sum_size += wsize
                count += 1
            count_list.append(count)
            lines = []
            for c in count_list:
                lines.append(" ".join((word for word in words[:c])))
                [words.pop(0) for x in range(0, c)]
            line_pos = pos
            for line in lines:
                render_text_line(message_img, color, font, line, pos=line_pos)
                line_pos = line_pos[0], line_pos[1] + word_height
        else:
            render_text_line(message_img, color, font, message, pos=pos)
        if self.titlebar:
            self.image.blit(message_img, (0, self.theight))
        else:
            self.image.blit(message_img, (0, 0))

    def close(self):
        self.rect.size = (0, 0)
        self.closed = True
        self.image = pygame.Surface((0, 0))
        self.draw_border()

    def expand(self):
        pass

    def set_scr_pos(self, position):
        self.scrpos = position
        size = self.rect.size
        bsize = (25, 25)
        if self.titlebar:
            bsize = self.theight, self.theight
        p1 = size[0] - bsize[0], 0
        p2 = size[0] - (2 * bsize[0]), 0
        sp1 = self.scrpos[0] + p1[0], self.scrpos[1]
        sp2 = self.scrpos[0] + p2[0], self.scrpos[1]
        self.titlebar.set_screen_pos(position)

    def draw_border(self):
        pygame.draw.rect(self.image, self.style['color'], ((0, 0), self.rect.size), 2)
        pygame.draw.rect(self.image, self.style['back-color'], ((0, 0), self.rect.size), 1)

    def set_position(self, position):
        self.rect.topleft = position

    def update(self):
        if self.titlebar.update():
            self.titlebar.draw(self.image)
            self.draw_border()
            return True
        return False

    def draw(self, surface):
        surface.blit(self.image, self.rect)
