import json
from lib import graphiclib
import xml.dom.minidom as minidom


class SpriteXmlParser:
    """Parses sprites from an xml-file"""

    def __init__(self, filename, screensize=(1000, 1000)):
        self.filename = filename
        self.screensize = screensize
        self._json_file = "./lib/configuration/sprites.json"
        self.xml = minidom.parse(filename)
        self.sprites = {}
        self.style = {
            'back-color': (0, 0, 0),
            'front-color': (255, 255, 255),
            'fontname': 'arial',
            'fontsize': 25,
            'position': (0, 0),
            'size': (100, 100),
            'opacity': 255,
            'index': 0,
            'flip-x': False,
            'flip-y': False,
            'maxsize': screensize
        }
        self._parse()
        self.spritelist = []
        [self.spritelist.extend(self.sprites[key]) for key in self.sprites]
        self.spritelist.sort(key=lambda tup: tup[1])
        self.spritelist = [sprite[0] for sprite in self.spritelist]

    # parses the sprites from the xml using a .json-
    # file with the stored information about each sprite type
    def _parse(self):
        tags = self.xml.getElementsByTagName('style')
        if len(tags) > 0:
            for tag in tags:
                style = self._get_tag_style(tag)
                self.style = style

        with open(self._json_file) as json_file:
            data = json.load(json_file)
            for sprite in data:
                self.sprites[sprite + 's'] = []
                for tag in self.xml.getElementsByTagName(sprite):
                    style = self._get_tag_style(tag)
                    attr = data[sprite]['attributes']
                    for var in attr:
                        vard = attr[var]
                        var_name = var.replace('-', '_')
                        execstr = "{}=tag.getAttribute('{}')".format(var_name, var)
                        exec(execstr)
                        evalstr = "len({})>0".format(var_name)
                        if eval(evalstr):
                            if vard['type'] != 'str':
                                execstr = "{}={}({})".format(var_name, vard['type'], var_name)
                                exec(execstr)
                        else:
                            if 'default' in vard:
                                if vard['type'] == 'str':
                                    execstr = "{}='{}'".format(var_name, str(vard['default']))
                                    exec(execstr)
                                else:
                                    execstr = "{}={}".format(var_name, str(vard['default']))
                                    exec(execstr)
                    if 'childs' in data[sprite]:
                        childs = data[sprite]['childs']
                        for child in childs:
                            childd = childs[child]
                            execstr = "{}s = []".format(child)
                            exec(execstr)
                            attr = childd["attributes"]
                            for var in attr:
                                execstr = "{}s=[t.getAttribute('{}') for t in tag.getElementsByTagName('{}')]".format(
                                    child, var, child)
                                exec(execstr)
                    exec("self.sprites[sprite+'s'].append(({},style['index']))".format(data[sprite]['class']))

    # STYLE PARSE
    def _get_tag_style(self, tag):
        # information
        style = self.style.copy()
        x_pos = tag.getAttribute('x')
        y_pos = tag.getAttribute('y')
        width = tag.getAttribute('width')
        height = tag.getAttribute('height')
        back_color_hex = tag.getAttribute('background-color').lstrip('#')
        front_color_hex = tag.getAttribute('front-color').lstrip('#')
        fontname = tag.getAttribute('fontname')
        fontsize = tag.getAttribute('fontsize')
        opacity = tag.getAttribute('opacity')
        index = tag.getAttribute('index')
        flip_x = tag.getAttribute('flip-x')
        flip_y = tag.getAttribute('flip-y')

        # parsing
        if len(back_color_hex) > 0:
            style['back-color'] = tuple(int(back_color_hex[i:i + 2], 16) for i in (0, 2, 4))
        if len(front_color_hex) > 0:
            style['front-color'] = tuple(int(front_color_hex[i:i + 2], 16) for i in (0, 2, 4))
        if len(fontname) > 0:
            if 'lib:' in fontname:
                fontname = fontname.replace(' ', '')
                fontname = fontname.replace('lib:', './lib/fonts/')
            style['fontname'] = fontname
        if len(fontsize) > 0:
            if fontsize[-1] is '%':
                fontsize = float(fontsize.rstrip('%')) / 100
                fontsize = self.screensize[1] * fontsize
            style['fontsize'] = int(fontsize)
        if len(x_pos) > 0:
            if x_pos[-1] is '%':
                x_pos = float(x_pos.rstrip('%')) / 100
                x_pos = self.screensize[0] * x_pos
            style['position'] = (int(x_pos), style['position'][1])
        if len(y_pos) > 0:
            if y_pos[-1] is '%':
                y_pos = float(y_pos.rstrip('%')) / 100
                y_pos = self.screensize[1] * y_pos
            style['position'] = (style['position'][0], int(y_pos))
        if len(width) > 0:
            if width[-1] is '%':
                width = float(width.rstrip('%')) / 100
                width = self.screensize[0] * width
            style['size'] = (int(width), style['size'][1])
        if len(height) > 0:
            if height[-1] is '%':
                height = float(height.rstrip('%')) / 100
                height = self.screensize[1] * height
            style['size'] = (style['size'][0], int(height))
        if len(opacity) > 0:
            opacity = int(float(opacity) * 255)
            style['opacity'] = opacity
        if len(index) > 0:
            index = int(index)
            style['index'] = index
        if len(flip_x) > 0:
            flip_x = 'true' in flip_x.lower()
            style['flip-x'] = flip_x
        if len(flip_y) > 0:
            flip_y = 'true' in flip_y.lower()
            style['flip-y'] = flip_y

        return style
