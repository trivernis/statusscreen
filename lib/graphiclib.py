import asyncio
import math
import subprocess as supc
import sys
import time
from datetime import datetime
from threading import Thread

import pygame
import pygame.camera
import pygame.transform as ptr

from lib import postgrelib as pglib
from lib import graphic_additions


# drawing stuff


def render_text_line(image, color, font, text, pos=(0, 0)):
    render_text = font.render(text, 1, color)
    image.blit(render_text, pos)


def test_sys_font(fontname):
    try:
        pygame.font.Font(fontname, 25)
        return False
    except OSError:
        pygame.font.SysFont(fontname, 25)
        return True


def render_lines(image, color, y_positions, width=1):
    rect = image.get_rect()
    rc_width = rect.width
    for y in y_positions:
        pygame.draw.line(image, color, (0, y), (rc_width, y), width)


def generate_error_image(message, size, font=None):
    if not font:
        font = pygame.font.SysFont('arial', int(size[1] / 10))
    image = pygame.Surface(size, pygame.SRCALPHA, 32)
    image = image.convert_alpha()
    points = []
    flip = False
    for x in range(0, size[0] + size[1]):
        if x % 20 == 0:
            if flip:
                points.append((x - size[1], size[1]))
                points.append((x, 0))
                flip = False
            else:
                points.append((x, 0))
                points.append((x - size[1], size[1]))
                flip = True

    pygame.draw.lines(image, (255, 0, 0), False, points)
    txtsize = font.size(message)
    topleft_x = (size[0] / 2) - (txtsize[0] / 2)
    topleft_y = (size[1] / 2) - (txtsize[1] / 2)
    rect = (topleft_x, topleft_y, txtsize[0], txtsize[1])
    pygame.draw.rect(image, (255, 0, 0), rect, 0)
    pos_ = ((size[0] / 2) - (txtsize[0] / 2), (size[1] / 2) - (txtsize[1] / 2))
    render_text_line(image, (0, 0, 0), font, message, pos=pos_)
    return image


def aspect_scale(img, size=(0, 0)):
    sx, sy = size
    scale_ = 0.0
    ix, iy = img.get_size()
    if ix > iy:
        scale_ = sx / ix
    else:
        scale_ = sy / iy
    ix = int(ix * scale_)
    iy = int(iy * scale_)
    return ptr.scale(img, (ix, iy))


# classes

class Line:
    def __init__(self, text, color=(0, 0, 0)):
        self.text = text
        self.color = color


# sprite-classes


class Clock(pygame.sprite.Sprite):
    """A Clock that displays the time in the HH:MM:SS-Format
     either in utc-time or normal time"""

    def __init__(self, style, utc=False):
        pygame.sprite.Sprite.__init__(self)
        self.style = style
        self.style_init()
        # additions
        self.utc = utc
        self.timestr = ""

    def style_init(self):
        self.size = self.style['size']
        self.fontname = self.style['fontname']
        self.color = self.style['front-color']
        self.bcolor = self.style['back-color']
        self.fontsize = self.style['fontsize']
        self.flip = (self.style['flip-x'], self.style['flip-y'])
        self.image = pygame.Surface(self.size)
        self.image.set_alpha(self.style['opacity'])
        self.image.fill(self.bcolor)
        self.rect = self.image.get_rect()
        self.rect.topleft = self.style['position']
        self.font = self._fit_font(self.fontname)

    def update(self, *args):
        if self.utc:
            time = datetime.utcnow()
        else:
            time = datetime.now()
        timestr = time.strftime("%H:%M:%S")
        if timestr != self.timestr:
            self.timestr = timestr
            self.image.fill(self.bcolor)
            render_text_line(self.image, self.color, self.font, timestr)
            pygame.draw.rect(self.image, self.color, ((0, 0), self.size), 1)
            if self.flip[0] or self.flip[1]:
                self.image = ptr.flip(self.image, self.flip[0], self.flip[1])

    def _fit_font(self, fontname):
        sysfont = test_sys_font(fontname)
        size = 1
        maxsize = self.size[1] * 2
        minsize = int(self.size[0] / 10)
        for x in range(minsize, maxsize):
            if sysfont:
                f = pygame.font.SysFont(fontname, x)
            else:
                f = pygame.font.Font(fontname, x)
            s = f.size("HH_MM_SS")
            if s[0] > self.rect.width or s[1] > self.rect.height:
                break
            size = x
        if sysfont:
            return pygame.font.SysFont(fontname, size)
        else:
            return pygame.font.Font(fontname, size)


# TODO: Add better hightlighting
class Console(pygame.sprite.Sprite):
    """A Console-Window that executes a given command via subprocess."""

    def __init__(self, style, commands, title=None, signalcolors=False):
        pygame.sprite.Sprite.__init__(self)
        self.style = style
        self.style_init()
        self.title = title
        self.sigcolors = signalcolors
        self.multiconsole = type(commands) is list
        self.error_img = generate_error_image("PROCESS FINISHED", self.size)
        if self.flip[0] or self.flip[1]:
            f_ = self.flip
            self.error_img = ptr.flip(self.error_img, f_[0], f_[1])
        self.ON_POSIX = 'posix' in sys.builtin_module_names
        self._titlebar_init()
        self._programInit(commands)

    def style_init(self):
        self.size = self.style['size']
        self.fontname = self.style['fontname']
        self.color = self.style['front-color']
        self.bcolor = self.style['back-color']
        self.fontsize = self.style['fontsize']
        self.flip = (self.style['flip-x'], self.style['flip-y'])
        self.image = pygame.Surface(self.size)
        self.image.set_alpha(self.style['opacity'])
        self.image.fill(self.bcolor)
        self.rect = self.image.get_rect()
        self.rect.topleft = self.style['position']
        if test_sys_font(self.fontname):
            self.font = pygame.font.SysFont(self.fontname, self.fontsize)
        else:
            self.font = pygame.font.Font(self.fontname, self.fontsize)

    def _titlebar_init(self):
        tstyle = {
            'color': self.bcolor,
            'back-color': self.color,
            'fontname': self.fontname,
            'fontsize': self.fontsize
        }
        bsize = (self.font.size('|')[1])
        size = self.size[0], bsize
        buttons = []
        buttons.append((graphic_additions.Buttons.close, self._clear_output))
        self.titlebar = graphic_additions.Titlebar((0, 0), size, screenpos=self.rect.topleft, buttons=buttons,
                                                   style=tstyle, title=self.title)

    def _programInit(self, commands):
        self.encoding = 'ISO-8859-1'
        self.proc = []
        self.q = asyncio.Queue()
        self.t = []
        std_ = supc.PIPE
        PSX_ = self.ON_POSIX
        if type(commands) is list:
            for cmd in commands:
                c_ = [com for com in cmd.split(' ')]
                proc = supc.Popen(c_, stdout=std_, bufsize=1, close_fds=PSX_)
                args_ = (proc.stdout, self.q)
                thread = Thread(target=self.enqueue_output, args=args_)
                self.proc.append(proc)
                self.t.append(thread)
                thread.daemon = True
                thread.start()
        else:
            c_ = [com for com in commands.split(' ')]
            proc = supc.Popen(c_, stdout=std_, bufsize=1, close_fds=PSX_)
            args_ = (proc.stdout, self.q)
            thread = Thread(target=self.enqueue_output, args=args_)
            self.proc.append(proc)
            self.t.append(thread)
            thread.daemon = True
            thread.start()
        self.lines = []
        self.finished = False
        self.lcount = round(self.size[1] / (self.font.size("gI|")[1] + 1))
        if self.title and not self.multiconsole:
            self.title = self.title + ' *Running*'

    def _clear_output(self):
        self.lines.clear()
        self.draw()

    def enqueue_output(self, out, queue):
        for line in iter(out.readline, b''):
            line = line.decode(self.encoding)
            queue.put_nowait(line.replace('\r', ' ').replace('\n', ' '))
        out.close()

    def update(self, *args):
        self.titlebar.update()
        if not self.multiconsole:
            if self.proc[0].poll() == 0 and not self.finished:
                if self.title:
                    self.title = self.title.replace('*Running*', '*Finished*')
                self.finished = True
        else:
            _closed = 0
            for proc in self.proc:
                if proc.poll() == 0:
                    if self.title:
                        if str(proc.pid) not in self.title:
                            if 'closed: ' not in self.title:
                                self.title += ' closed: '
                            self.title += str(proc.pid) + ', '
                    _closed += 1
            if _closed == len(self.proc):
                self.finished = True
        if self.finished and self.q.empty() and len(self.proc) > 0:
            self.image.blit(self.error_img, (0, 0))
            self.proc = []
        if not self.q.empty():
            stdout = self.q.get_nowait()
            if len(self.lines) > self.lcount and not self.title:
                self.lines.pop(0)
            elif len(self.lines) > self.lcount:
                self.lines.pop(1)
            if len(stdout) > 0:
                if self.sigcolors:
                    cl_ = self.parse_color(stdout)
                    self.lines.append(Line(stdout, color=cl_))
                else:
                    self.lines.append(Line(stdout, color=self.color))
                self.draw()
                if self.flip[0] or self.flip[1]:
                    f_ = self.flip
                    self.image = ptr.flip(self.image, f_[0], f_[1])
        self.titlebar.draw(self.image)

    def parse_color(self, string):
        string = string.replace(' ', '')
        if len(string) > 0:
            if '[debug]' in string.lower():
                return (150, 255, 255)
            elif '[info]' in string.lower():
                return (0, 255, 0)
            elif '[warning]' in string.lower():
                return (255, 255, 0)
            elif '[error]' in string.lower():
                return (255, 50, 50)
            elif '[critical]' in string.lower():
                return (255, 0, 0)
            elif 'traceback' in string.lower():
                return (255, 0, 255)
            elif string[0] == '-' and string[-1] == '-':
                return (0, 255, 255)
            elif string[0] == '#' and string[-1] == '#':
                return (255, 255, 0)
            else:
                return self.color
        else:
            return self.color

    def draw(self):
        """Draws the lines in the way,
        that the last line is allways on the bottom"""
        font = self.font
        draw_height = 0
        linecount = 0
        endreached = False
        self.image.fill(self.bcolor)
        txtsize = self.font.size('|')
        limage = pygame.Surface((self.size[0], txtsize[1]))
        console_width = self.size[0]
        console_height = self.size[1] - txtsize[1]
        cimage = pygame.Surface((console_width, console_height))

        for line in reversed(self.lines):
            text = line.text
            color = line.color
            text_width, text_height = font.size(text)

            if not endreached:
                linecount += 1

            if text_width > console_width:
                subtext = ""
                textparts = []
                for char in text:
                    if font.size(subtext + char)[0] > console_width:
                        textparts.append(subtext)
                        subtext = ""
                        subtext += char
                    else:
                        subtext += char
                textparts.append(subtext)
                count = 1
                for textpart in reversed(textparts):
                    pos_y = console_height - ((count * text_height) + draw_height)
                    if pos_y < 0:
                        endreached = True
                        break
                    p_ = (0, pos_y)
                    render_text_line(cimage, color, font, textpart, pos=p_)
                    count += 1
                draw_height += len(textparts) * text_height
                if endreached:
                    break
            else:
                draw_height += text_height
                if draw_height > console_height:
                    break
                p_ = (0, console_height - draw_height)
                render_text_line(cimage, color, font, text, pos=p_)
        self.image.blit(cimage, (0, txtsize[1]))
        if self.title:
            self.titlebar.set_title(self.title)
            self.titlebar.draw(self.image)
        c_ = self.color
        pygame.draw.rect(self.image, c_, pygame.Rect((0, 0), self.size), 1)

    def __del__(self):
        for proc in self.proc:
            proc.kill()


class Label(pygame.sprite.Sprite):
    """A Label to show some text on"""

    def __init__(self, style, text, mode='text', interval=1):
        pygame.sprite.Sprite.__init__(self)
        self.style = style
        self.style_init()
        # additions
        self.text = text
        self.mode = mode
        self.updated = True
        self.lastupdate = 0
        self.interval = interval

    def style_init(self):
        self.size = self.style['size']
        self.fontname = self.style['fontname']
        self.color = self.style['front-color']
        self.bcolor = self.style['back-color']
        self.fontsize = self.style['fontsize']
        self.flip = (self.style['flip-x'], self.style['flip-y'])
        self.image = pygame.Surface(self.size)
        self.image.set_alpha(self.style['opacity'])
        self.image.fill(self.bcolor)
        self.rect = self.image.get_rect()
        self.rect.topleft = self.style['position']
        if test_sys_font(self.fontname):
            self.font = pygame.font.SysFont(self.fontname, self.fontsize)
        else:
            self.font = pygame.font.Font(self.fontname, self.fontsize)

    def update(self, *args):
        lu_ = self.lastupdate
        m_ = self.mode
        refreshable = ((time.time() - lu_) > self.interval and m_ == 'scroll')
        if self.updated or refreshable:
            if self.mode == 'scroll' and len(self.text) > 0:
                self.text = self.text[1::] + self.text[0]
            self.image.fill(self.bcolor)
            render_text_line(self.image, self.color, self.font, self.text)
            pygame.draw.rect(self.image, self.color, ((0, 0), self.size), 1)
            self.lastupdate = time.time()
            self.updated = False
            if self.flip[0] or self.flip[1]:
                f_ = self.flip
                self.image = ptr.flip(self.image, f_[0], f_[1])

    def set_text(self, text):
        if text != self.text:
            self.text = text
            self.updated = True


class SQLValueLabel(pygame.sprite.Sprite):
    """A Label that shows a SQL-Value from a SQL-server"""

    def __init__(self, style, connection, tablename, columnname, interval=10):
        pygame.sprite.Sprite.__init__(self)
        self.style = style
        self.style_init()
        # additions
        self.update_interval = interval
        self.updated = False
        self.table = tablename
        self.column = columnname
        self.connection = connection
        self.cursor = connection.cursor()
        self.lastupdate = 0

    def style_init(self):
        self.size = self.style['size']
        self.fontname = self.style['fontname']
        self.color = self.style['front-color']
        self.bcolor = self.style['back-color']
        self.fontsize = self.style['fontsize']
        self.flip = (self.style['flip-x'], self.style['flip-y'])
        self.image = pygame.Surface(self.size)
        self.image.set_alpha(self.style['opacity'])
        self.image.fill(self.bcolor)
        self.rect = self.image.get_rect()
        self.rect.topleft = self.style['position']
        if test_sys_font(self.fontname):
            self.font = pygame.font.SysFont(self.fontname, self.fontsize)
        else:
            self.font = pygame.font.Font(self.fontname, self.fontsize)

    def update(self, *args):
        if (time.time() - self.lastupdate) > self.update_interval:
            cl = self.color
            self.image.fill(self.bcolor)
            sql_ = 'SELECT {} FROM {} ORDER BY id DESC LIMIT 1;'
            self.cursor.execute(sql_.format(self.column, self.table))
            value = self.cursor.fetchone()[0]
            render_text_line(self.image, cl, self.font, str(value))
            pygame.draw.rect(self.image, cl, ((0, 0), self.size), 1)
            self.lastupdate = time.time()
            if self.flip[0] or self.flip[1]:
                f_ = self.flip
                self.image = ptr.flip(self.image, f_[0], f_[1])


class SQLGraph(pygame.sprite.Sprite):
    """A Label that shows a SQL-Value from a SQL-server"""

    def __init__(self, style, tablename, columnname, update_interval=10, width=1, label=True, x_scale=1.0, unit='',
                 title=None, database='default', group='POSTGRESQL'):
        pygame.sprite.Sprite.__init__(self)
        # standards
        self.style = style
        self.style_init()
        # additions
        self.width = width
        self.title = title
        self.group = group
        self.update_interval = update_interval
        self.label = label
        self.x_scale = x_scale
        self.unit = unit
        self.database = database
        self.updated = False
        self.table = tablename
        self.column = columnname
        self.lastupdate = 0
        self.maximized = False
        self.error_img = generate_error_image("CONNECTION CLOSED", self.size)
        self._titlebar_init()
        self.sql_connect()

    def style_init(self):
        self.size = self.style['size']
        self.fontname = self.style['fontname']
        self.color = self.style['front-color']
        self.bcolor = self.style['back-color']
        self.fontsize = self.style['fontsize']
        self.flip = (self.style['flip-x'], self.style['flip-y'])
        self.image = pygame.Surface(self.size)
        self.image.set_alpha(self.style['opacity'])
        self.image.fill(self.bcolor)
        self.rect = self.image.get_rect()
        self.rect.topleft = self.style['position']
        self.position = self.style['position']
        self.maxsize = self.style['maxsize']
        if test_sys_font(self.fontname):
            self.font = pygame.font.SysFont(self.fontname, self.fontsize)
        else:
            self.font = pygame.font.Font(self.fontname, self.fontsize)

    def _titlebar_init(self):
        tstyle = {
            'color': self.bcolor,
            'back-color': self.color,
            'fontname': self.fontname,
            'fontsize': self.fontsize
        }
        bsize = (self.font.size('|')[1])
        maxsize = self.maxsize[0], bsize
        buttons = []
        buttons.append((graphic_additions.Buttons.max, self.maximize))
        buttons.append((graphic_additions.Buttons.refresh, self.draw))
        self.titlebar = graphic_additions.Titlebar((0, 0), (self.size[0], bsize), screenpos=self.position,
                                                   buttons=buttons, style=tstyle, title=self.title, maxsize=maxsize)

    def sql_connect(self):
        try:
            n_ = 'statusscreen'
            d_ = self.database
            g_ = self.group
            self.connection, self.cursor = pglib.login(d_, name=n_, config=g_)
        except Exception as e:
            print(e)

    def update(self, *args):
        if (time.time() - self.lastupdate) > self.update_interval:
            self.draw()
        self.titlebar.update()
        self.titlebar.draw(self.image)

    def maximize(self):
        if not self.maximized:
            self.maximized = True
            self.rect.size = self.maxsize
            img = aspect_scale(self.image, self.maxsize)
            self.image = pygame.Surface(self.maxsize)
            self.image.blit(img, (0, 0))
            self.titlebar.set_max(True)
            self.rect.topleft = (0, 0)
            self.draw()
        else:
            self.maximized = False
            self.rect.size = self.size
            img = aspect_scale(self.image, self.size)
            self.image = pygame.Surface(self.size)
            self.image.blit(img, (0, 0))
            self.titlebar.set_max(False)
            self.rect.topleft = self.position
            self.draw()

    def draw(self):
        size = self.rect.size
        try:
            self.image.fill(self.bcolor)
            self.cursor.execute('SELECT {} FROM {} ORDER BY id DESC LIMIT {};'.format(self.column, self.table,
                                                                                      math.ceil(
                                                                                          size[0] / self.x_scale)))
            graphsize = (size[0], size[1] - (self.font.size('|')[1] + 2))
            graphimg = pygame.Surface(graphsize)
            res = self.cursor.fetchall()
            values = [_i for _i in reversed([row[0] for row in res])]
            if self.x_scale < 1:
                newres = []
                factor = int(math.ceil(len(values) / size[0]))
                for i in range(0, size[0]):
                    try:
                        value = values[i * factor]
                    except IndexError:
                        value = values[-1]
                    vsum = value
                    for _v in range(1, factor):
                        try:
                            vsum += values[i * factor + _v]
                        except Exception as ex:
                            print("Exception in sqlgraphs update:{}".format(ex))
                            vsum += value
                    value = vsum / factor
                    newres.append(value)
                values = newres
            max_value = max(values)
            min_value = min(values)
            pointlist = [graphsize, (0, graphsize[1])]
            if max_value != min_value:
                scale = graphsize[1] / (max_value - min_value)
            else:
                scale = graphsize[1] / max_value
            count = 0
            if self.x_scale < 1:
                for value in values:
                    pointlist.append((count, graphsize[1] - ((value - min_value) * scale)))
                    count += 1
            else:
                for value in values:
                    pointlist.append((count, graphsize[1] - ((value - min_value) * scale)))
                    for i in range(0, int(math.ceil(self.x_scale))):
                        count += 1
            # graph
            pygame.draw.polygon(graphimg, self.color, pointlist, self.width)
            self.image.blit(graphimg, (0, self.font.size('|')[1] + 1))
            # label
            if self.label or self.title:
                if self.title:
                    txt = '{} max:{}{},min:{}{},last:{}{}'.format(self.title, round(max_value, 1), self.unit,
                                                                  round(min_value, 1), self.unit,
                                                                  round(values[len(values) - 1], 1), self.unit)
                else:
                    txt = 'max:{}{},min:{}{},last:{}{}'.format(round(max_value, 1), self.unit, round(min_value, 1),
                                                               self.unit, round(values[len(values) - 1], 1), self.unit)
                self.titlebar.set_title(txt)
            pygame.draw.rect(self.image, self.color, pygame.Rect((0, 0), size), 1)
            self.lastupdate = time.time()
        except Exception as e:
            print(e)
            self.image.blit(self.error_img, (0, 0))
            self.lastupdate = time.time() - 5
            self.sql_connect()
            pygame.draw.rect(self.image, (255, 0, 0), pygame.Rect((0, 0), size), 1)

        if self.flip[0] or self.flip[1]:
            self.image = pygame.transform.flip(self.image, self.flip[0], self.flip[1])


class Timer(pygame.sprite.Sprite):
    """A Clock that displays the time in the HH:MM:SS-Format either in utc-time or normal time"""

    def __init__(self, style, datetimestring, format="%d.%m.%Y %H:%M:%S", repeat=None, title=None):
        pygame.sprite.Sprite.__init__(self)
        # standards
        self.style = style
        self.style_init()
        # additions
        self.end = datetime.strptime(datetimestring, format)
        self.title = title
        self.repeat = repeat
        self.font = self._fit_font(self.fontname)

    def style_init(self):
        self.size = self.style['size']
        self.fontname = self.style['fontname']
        self.color = self.style['front-color']
        self.bcolor = self.style['back-color']
        self.fontsize = self.style['fontsize']
        self.flip = (self.style['flip-x'], self.style['flip-y'])
        self.image = pygame.Surface(self.size)
        self.image.set_alpha(self.style['opacity'])
        self.image.fill(self.bcolor)
        self.rect = self.image.get_rect()
        self.rect.topleft = self.style['position']
        self.laststr = ""

    def update(self, *args):
        remaining = self.end - datetime.now()
        timestr = str(remaining).split(".")[0]
        if timestr != self.laststr:
            txtsize = (0, 0)
            ttlimg = None
            # title definition
            if self.title:
                fn_ = self.fontname
                fs_ = self.fontsize
                if test_sys_font(self.fontname):
                    ttlfont = pygame.font.SysFont(fn_, fs_)
                else:
                    ttlfont = pygame.font.Font(fn_, fs_)
                txtsize = ttlfont.size(self.title)
                c_size = (self.size[0], self.size[1] - txtsize[1])
                clockimg = pygame.Surface(c_size)
                ttlimg = pygame.Surface((self.size[0], txtsize[1]))
                ttlimg.fill(self.color)
                render_text_line(ttlimg, self.bcolor, ttlfont, self.title)
            else:
                clockimg = pygame.Surface(self.size)
            clockimg.fill(self.bcolor)
            if self.repeat:
                if remaining.total_seconds() < 0:
                    repeat = self.repeat
                    end = self.end
                    year = end.year
                    month = end.month
                    day = end.day
                    hour = end.hour
                    minute = end.minute
                    second = end.second
                    microsecond = end.microsecond

                    if 'y' in repeat:
                        year += int(repeat.replace('y', ''))
                    elif 'm' in repeat:
                        month += int(repeat.replace('m', ''))
                    elif 'd' in repeat:
                        month += int(repeat.replace('m', ''))
                    elif 'H' in repeat:
                        hour += int(repeat.replace('H', ''))
                    elif 'M' in repeat:
                        minute += int(repeat.replace('M', ''))
                    elif 'S' in repeat:
                        second += int(repeat.replace('S', ''))
                    self.end = datetime(year=year, month=month, day=day, hour=hour, minute=minute, second=second,
                                        microsecond=microsecond)
                    self.font = self._fit_font(self.fontname)
            # rendering
            render_text_line(clockimg, self.color, self.font, timestr)
            pygame.draw.rect(clockimg, self.color, ((0, 0), self.size), 1)
            if self.title:
                self.image.blit(clockimg, (0, txtsize[1]))
                if ttlimg:
                    self.image.blit(ttlimg, (0, 0))
            else:
                self.image.blit(clockimg, (0, 0))

            if self.flip[0] or self.flip[1]:
                f_ = self.flip
                self.image = ptr.flip(self.image, f_[0], f_[1])

    def _fit_font(self, fontname):
        fitstr = str(self.end - datetime.now()).split(".")[0]
        sysfont = test_sys_font(fontname)
        size = 1
        maxsize = self.size[1] * 2
        minsize = int(self.size[0] / 10)
        for x in range(minsize, maxsize):
            if sysfont:
                f = pygame.font.SysFont(fontname, x)
            else:
                f = pygame.font.Font(fontname, x)
            s = f.size(fitstr)
            if s[0] > self.rect.width or s[1] > self.rect.height:
                break
            size = x
        if sysfont:
            return pygame.font.SysFont(fontname, size)
        else:
            return pygame.font.Font(fontname, size)


class Image(pygame.sprite.Sprite):

    def __init__(self, style, source, title=None, keep_ratio=False, interval=None):
        pygame.sprite.Sprite.__init__(self)
        self.style = style
        self.style_init()
        # additions
        self.source = source
        self.title = title
        self.keep_ratio = keep_ratio
        self.interval = interval
        self.last_update = time.time()
        self.error_img = generate_error_image('COULD NOT LOAD MEDIA', self.size)
        try:
            self.draw()
        except pygame.error:
            self.image.blit(self.error_img, (0, 0))

    def style_init(self):
        self.size = self.style['size']
        self.fontname = self.style['fontname']
        self.color = self.style['front-color']
        self.bcolor = self.style['back-color']
        self.fontsize = self.style['fontsize']
        self.flip = (self.style['flip-x'], self.style['flip-y'])
        self.image = pygame.Surface(self.size)
        self.image.set_alpha(self.style['opacity'])
        self.image.fill(self.bcolor)
        self.rect = self.image.get_rect()
        self.rect.topleft = self.style['position']
        if test_sys_font(self.fontname):
            self.font = pygame.font.SysFont(self.fontname, self.fontsize)
        else:
            self.font = pygame.font.Font(self.fontname, self.fontsize)

    def draw(self, *args):
        txtimg = None
        picimg = None
        txtsize = (0, 0)

        if self.title:
            txtsize = self.font.size(self.title)
            txtimg = pygame.Surface((self.size[0], txtsize[1]))
            picimg = pygame.Surface((self.size[0], self.size[1] - txtsize[1]))
        else:
            picimg = pygame.Surface(self.size)

        picture = pygame.image.load(self.source)
        if self.keep_ratio:
            picture_size = (picture.get_width(), picture.get_height())
            if self.size[0] < picture_size[0]:
                scale = self.size[0] / picture_size[0]
                picture_size = (picture_size[0] * scale, picture_size[1] * scale)
            if self.size[1] < picture_size[1]:
                scale = self.size[1] / picture_size[1]
                picture_size = (picture_size[0] * scale, picture_size[1] * scale)
            picture_size = (int(picture_size[0]), int(picture_size[1]))
            picimg.blit(pygame.transform.scale(picture, picture_size), (0, 0))
        else:
            picimg.blit(pygame.transform.scale(picture, (picimg.get_width(), picimg.get_height())), (0, 0))
        if self.title:
            txtimg.fill(self.color)
            render_text_line(txtimg, self.bcolor, self.font, self.title)
            self.image.blit(picimg, (0, txtsize[1]))
            self.image.blit(txtimg, (0, 0))
        else:
            self.image.blit(picimg, (0, 0))
        self.last_update = time.time()
        if self.flip[0] or self.flip[1]:
            f_ = self.flip
            self.image = ptr.flip(self.image, f_[0], f_[1])

    def update(self, *args):
        if self.interval:
            if (time.time() - self.last_update) > self.interval:
                try:
                    self.draw()
                except pygame.error:
                    self.image.blit(self.error_img, (0, 0))


class Webcam(pygame.sprite.Sprite):

    def __init__(self, style, title=None, interval=0.1, number=0):
        pygame.sprite.Sprite.__init__(self)
        self.style = style
        self.style_init()
        # additions
        self.title = title
        self.interval = interval
        self.last_update = 0
        self.error_img = generate_error_image('WEBCAM INITIALIZING ERROR', self.size)
        try:
            c_ = pygame.camera.list_cameras()[number]
            self.camera = pygame.camera.Camera(c_, self.size)
            self.camera.start()
        except Exception as ex:
            print("Exception in camera init: {}".format(ex))
            self.image.blit(self.error_img, (0, 0))

    def style_init(self):
        self.size = self.style['size']
        self.fontname = self.style['fontname']
        self.color = self.style['front-color']
        self.bcolor = self.style['back-color']
        self.fontsize = self.style['fontsize']
        self.flip = (self.style['flip-x'], self.style['flip-y'])
        self.image = pygame.Surface(self.size)
        self.image.set_alpha(self.style['opacity'])
        self.image.fill(self.bcolor)
        self.rect = self.image.get_rect()
        self.rect.topleft = self.style['position']
        if test_sys_font(self.fontname):
            self.font = pygame.font.SysFont(self.fontname, self.fontsize)
        else:
            self.font = pygame.font.Font(self.fontname, self.fontsize)

    def update(self, *args):
        if (time.time() - self.last_update) > self.interval:
            try:
                txtimg = None
                txtsize = (0, 0)
                if self.title:
                    txtsize = self.font.size(self.title)
                    txtimg = pygame.Surface((self.size[0], txtsize[1]))
                    camimg = pygame.Surface((self.size[0], self.size[1] - txtsize[1]))
                else:
                    camimg = pygame.Surface(self.size)
                self.camera.get_image(camimg)
                if self.title:
                    txtimg.fill(self.color)
                    render_text_line(txtimg, self.bcolor, self.font, self.title)
                    self.image.blit(camimg, (0, txtsize[1]))
                    self.image.blit(txtimg, (0, 0))
                else:
                    self.image.blit(camimg, (0, 0))
                self.last_update = time.time()
            except Exception as e:
                print("Exception in camera update: {}".format(e))
                self.image.blit(self.error_img, (0, 0))
            if self.flip[0] or self.flip[1]:
                f_ = self.flip
                self.image = ptr.flip(self.image, f_[0], f_[1])

    def __del__(self):
        self.camera.stop()


class NotificationCenter(pygame.sprite.Sprite):
    """A Center with notification cards"""

    def __init__(self, style, commands, title='', signalcolors=False, notification_height=200):
        pygame.sprite.Sprite.__init__(self)
        self.style = style
        self.notifications = []
        self.sigcolors = signalcolors
        self.multiconsole = type(commands) is list
        self.noti_height = notification_height
        self.__style_init()
        self.__title_init(title)
        self.__program_init(commands)

    def __style_init(self):
        style = self.style
        self.size = style['size']
        self.fontname = style['fontname']
        self.color = style['front-color']
        self.bcolor = style['back-color']
        self.fontsize = style['fontsize']
        self.flip = (style['flip-x'], style['flip-y'])
        self.image = pygame.Surface(self.size)
        self.image.set_alpha(style['opacity'])
        self.image.fill(self.bcolor)
        self.rect = self.image.get_rect()
        self.rect.topleft = style['position']
        self.position = style['position']
        self.noti_style = {
            'color': style['back-color'],
            'back-color': style['front-color'],
            'fontname': style['fontname'],
            'fontsize': style['fontsize']
        }

    def __title_init(self, title):
        style = self.style
        tbar_style = {
            'color': style['back-color'],
            'back-color': style['front-color'],
            'fontname': style['fontname'],
            'fontsize': math.floor(style['fontsize'] * 1.2)
        }
        if test_sys_font(style['fontname']):
            tbar_font = pygame.font.SysFont(style['fontname'], tbar_style['fontsize'])
        else:
            tbar_font = pygame.font.Font(style['fontname'], tbar_style['fontsize'])
        bar_size = self.size[0], tbar_font.size('|')[1] + 4
        buttons = [(graphic_additions.Buttons.close, self.clear)]
        self.titlebar = graphic_additions.Titlebar((0, 0), bar_size, buttons, style=tbar_style, screenpos=self.position,
                                                   title=title)
        self.titlebar.draw(self.image)

    def __program_init(self, commands):
        self.encoding = 'ISO-8859-1'
        self.ON_POSIX = 'posix' in sys.builtin_module_names
        self.proc = []
        self.q = asyncio.Queue()
        self.t = []
        std_ = supc.PIPE
        PSX_ = self.ON_POSIX
        if type(commands) is list:
            for cmd in commands:
                c_ = [com for com in cmd.split(' ')]
                proc = supc.Popen(c_, stdout=std_, bufsize=1, close_fds=PSX_)
                args_ = (proc.stdout, self.q, proc)
                thread = Thread(target=self.enqueue_output, args=args_)
                self.proc.append(proc)
                self.t.append(thread)
                thread.daemon = True
                thread.start()
        else:
            c_ = [com for com in commands.split(' ')]
            proc = supc.Popen(c_, stdout=std_, bufsize=1, close_fds=PSX_)
            args_ = (proc.stdout, self.q, proc)
            thread = Thread(target=self.enqueue_output, args=args_)
            self.proc.append(proc)
            self.t.append(thread)
            thread.daemon = True
            thread.start()
        self.lines = []
        self.finished = False

    def clear(self):
        self.notifications = []
        self._refr_noti()
        self.draw()

    def parse_color(self, string):
        string = string.replace(' ', '')
        if len(string) > 0:
            if '[debug]' in string.lower():
                return (150, 255, 255), 'Debug'
            elif '[info]' in string.lower():
                return (0, 255, 0), 'Info'
            elif '[warning]' in string.lower():
                return (255, 255, 0), 'Warning'
            elif '[error]' in string.lower():
                return (255, 50, 50), 'Error'
            elif '[critical]' in string.lower():
                return (255, 0, 0), 'Critical'
            elif 'traceback' in string.lower():
                return (255, 0, 255), 'Traceback'
            elif string[0] == '-' and string[-1] == '-':
                return (0, 255, 255), 'title'
            elif string[0] == '#' and string[-1] == '#':
                return (255, 255, 0), 'date'
            else:
                return self.color, ''
        else:
            return self.color, ''

    def add_notification(self, message, title):
        size = self.size
        nsize = size[0], self.noti_height
        position = 0, self.size[1] - self.noti_height
        scrpos = self.position[0], self.position[1] + position[1]
        nt = graphic_additions.NotificationCard(position, nsize, message, title=title, style=self.noti_style.copy(),
                                                screenpos=scrpos)
        self.notifications.append(nt)
        self._refr_noti()

    def enqueue_output(self, out, queue, proc):
        for line in iter(out.readline, b''):
            line = line.decode(self.encoding)
            queue.put_nowait((line.replace('\r', ' ').replace('\n', ' '), proc))
        out.close()

    def _refr_noti(self):
        position = 0, self.size[1] - self.noti_height
        scrpos = self.position[0], self.position[1] + position[1]
        for notification in reversed(self.notifications):
            if position[1] + (5 * self.noti_height) < 0:
                self.notifications.remove(notification)
            else:
                notification.set_scr_pos(scrpos)
                notification.set_position(position)
            position = 0, position[1] - self.noti_height
            scrpos = scrpos[0], scrpos[1] - self.noti_height
        self.draw()

    def draw(self):
        self.image.fill(self.style['back-color'])
        for noti in self.notifications:
            noti.draw(self.image)
        self.titlebar.draw(self.image)
        if self.flip[0] or self.flip[1]:
            f_ = self.flip
            self.image = ptr.flip(self.image, f_[0], f_[1])

    def update(self, *args):
        self.titlebar.update()
        if not self.q.empty():
            out, proc = self.q.get_nowait()
            if len(out.strip(' ')) > 0:
                title = ''
                if self.sigcolors:
                    cl, title = self.parse_color(out)
                    if title == 'title':
                        title = out
                        try:
                            out, proc = self.q.get_nowait()
                            while len(out.strip(' ')) == 0:
                                out, proc = self.q.get_nowait()
                            _, ttl = self.parse_color(out)
                            if ttl == 'title':
                                out = title
                        except asyncio.QueueEmpty:
                            pass
                    self.noti_style['back-color'] = cl
                if self.multiconsole:
                    title = "Process {}: {}".format(proc.pid, title)
                self.add_notification(out, title)
        updated = False
        for noti in list(self.notifications):
            if noti.update():
                updated = True
            if noti.closed:
                self.notifications.remove(noti)
                updated = True
        if updated:
            self._refr_noti()
            self.draw()
