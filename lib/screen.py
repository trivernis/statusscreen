import pygame
from pygame import FULLSCREEN, DOUBLEBUF, HWSURFACE, RESIZABLE

pygame.display.init()
pygame.font.init()


class Screen:
    def __init__(self, size=(-1, -1), fullscreen=True):
        self.screen = None
        self.font_family = './lib/fonts/ConsoleFu.otf'
        try:
            self.font = pygame.font.Font(self.font_family, 25)
        except OSError:
            self.font = pygame.font.SysFont(self.font_family, 25)
        self.size = size
        self._display_init(fullscreen=fullscreen)
        pygame.font.init()

    def _display_init(self, fullscreen=True):
        pygame.display.init()
        displayinfo = pygame.display.Info()
        if self.size == (-1, -1):
            self.size = (displayinfo.current_w, displayinfo.current_h)
        if fullscreen:
            fl = FULLSCREEN | DOUBLEBUF
            self.screen = pygame.display.set_mode(self.size, fl)
        else:
            self.screen = pygame.display.set_mode(self.size)
        self.fullscreen = True
        self.screen.set_alpha(None)
        self.screen.fill((255, 255, 255))

    def refresh(self, rectangles=None):
        if rectangles:
            pygame.display.update(rectangles)
        else:
            pygame.display.flip()

    def drawBG(self):
        self.screen.fill((255, 255, 255))

    def toggle_fullscreen(self):
        if self.fullscreen:
            pygame.display.set_mode(self.size, HWSURFACE | DOUBLEBUF | RESIZABLE)
            self.fullscreen = False
        else:
            pygame.display.set_mode(self.size, FULLSCREEN | DOUBLEBUF)
            self.fullscreen = True
