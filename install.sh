#!/usr/bin/env bash
sudo apt update
aptdepts=(python3-bs4)
for adep in ${aptdepts[*]};do
    eval sudo apt install $adep
done
pipdepts=(pygame bs4 feedparser configparser optparse psycopg2)
p=""
if hash pip3; then
    echo "installing with pip3"
    p="sudo pip3 install"
elif hash pip; then
    echo "installing with pip"
    p="sudo pip install"
elif hash python3; then
    echo "installing with python3 -m pip"
    p="sudo python3 -m pip install"
elif hash python; then
    echo "installing with python -m pip"
    p="sudo python -m pip install"
else
    echo "did not find any pip install command"
    exit 1
fi

for dep in ${pipdepts[*]};do
    eval $p $dep
done
exit 0