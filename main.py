import configparser
import logging
import os
import sys
import time
from optparse import OptionParser

import pygame
import pygame.camera

from lib import screen, sprite_parser

logger = logging.getLogger(__name__)


def label_mode(labels, clock, runtime):
    for label in labels:
        if label.mode == 'fps':
            label.set_text(str(round(clock.get_fps(), 1)))
        elif label.mode == 'runtime':
            label.set_text(str(round(runtime)))


def main(options):
    try:
        pygame.camera.init()
    except Exception as e:
        print("Couldnt Initialize Camera {}".format(e))

    settings = configparser.ConfigParser()
    if os.path.isfile('./config/Settings.ini'):
        settings.read('./config/Settings.ini')
    else:
        settings.read('./config/DefaultSettings.ini')

    if options.height and options.width:
        size_ = (options.width, options.height)
        scr = screen.Screen(fullscreen=False, size=size_)
    else:
        scr = screen.Screen()

    all_sprites = pygame.sprite.RenderUpdates()
    framerate = int(settings['Default']['framerate'])
    filename = './interfaces/interface.xml'

    if options.file:
        filename = './interfaces/' + options.file
    parser = sprite_parser.SpriteXmlParser(filename, screensize=scr.size)

    sprites = parser.spritelist
    all_sprites.add(sprites)
    logger.debug('found {} sprites'.format(len(all_sprites)))
    labels = [sprite[0] for sprite in parser.sprites['labels']]
    clock = pygame.time.Clock()
    running = True
    restart = False
    fore_sprites = pygame.sprite.RenderUpdates()
    duration = count = 0

    # the start of the loop
    while running:
        clock.tick(framerate)
        start = time.time()
        # eventloop
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                # close at ALT+F4
                if event.key == pygame.K_F4 and pygame.key.get_mods() and pygame.KMOD_ALT:
                    running = False
                # minimize the sprite
                elif event.key == pygame.K_ESCAPE:
                    for sprite in list(fore_sprites):
                        if hasattr(sprite, 'maximize'):
                            sprite.maximize()
                            fore_sprites.remove(sprite)
                elif event.key == pygame.K_F11:
                    scr.toggle_fullscreen()
                elif event.key == pygame.K_F5:
                    running = False
                    restart = True
            elif not scr.fullscreen and event.type == pygame.VIDEORESIZE:
                scr.screen = pygame.display.set_mode(event.dict['size'],
                                                     pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE)

        for sprite in sprites:
            if hasattr(sprite, 'maximized'):
                if sprite.maximized:
                    if sprite not in fore_sprites:
                        fore_sprites.add(sprite)
                else:
                    if sprite in fore_sprites:
                        fore_sprites.remove(sprite)
        # screenstuff
        updateRects = []
        label_mode(labels, clock, time.time() - start)
        all_sprites.update()
        updateRects.extend(all_sprites.draw(scr.screen))
        fore_sprites.update()
        updateRects.extend(fore_sprites.draw(scr.screen))
        scr.refresh(rectangles=updateRects)
        duration += time.time() - start
        count += 1

    # end
    print("Average loop duration {}s".format(duration / count))
    killed = []
    for sprite in all_sprites:
        try:
            if hasattr(sprite, 'proc'):
                for proc in sprite.proc:
                    proc.kill()
                    killed.append(proc.pid)
        except Exception as e:
            print("Exception in proc_kill {}".format(e))
    if len(killed) > 0:
        print('killed {}'.format(killed))
    pygame.quit()
    return restart


if __name__ == '__main__':
    parser = OptionParser()
    # adding arguments
    h_ = "a custom height for the window"
    parser.add_option("-H", "--height", type="int", dest="height", help=h_)

    h_ = "a custom width for the window"
    parser.add_option("-W", "--width", type="int", dest="width", help=h_)

    h_ = "the name of the interface-file"
    parser.add_option("-f", "--file", type="string", dest="file", help=h_)

    del h_
    (options, args) = parser.parse_args()
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    try:
        restart = main(options)
        while restart:
            pygame.font.init()
            pygame.display.init()
            restart = main(options)
    except KeyboardInterrupt:
        pygame.quit()
    except Exception as e:
        logger.exception(e)
    finally:
        sys.exit()
